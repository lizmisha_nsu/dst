CREATE TABLE book(id serial PRIMARY KEY, title TEXT);

CREATE TABLE author(id serial PRIMARY KEY, name varchar(50));

CREATE TABLE subject(id serial PRIMARY KEY, name varchar(50));

CREATE TABLE publisher(id serial PRIMARY KEY, name varchar(100));

CREATE TABLE book_to_subject(id serial PRIMARY KEY, book_id INTEGER REFERENCES book(id),
subject_id INTEGER REFERENCES subject(id));

CREATE TABLE book_to_author(id serial PRIMARY KEY, book_id INTEGER REFERENCES book(id),
author_id INTEGER REFERENCES author(id));

CREATE TABLE book_info(id serial PRIMARY KEY, isbn10 varchar(10), isbn13 varchar(13),
book_id INTEGER REFERENCES book(id), publisher_id INTEGER REFERENCES publisher(id),
pubyear INTEGER, price FLOAT, pages FLOAT, width FLOAT, height FLOAT, depth FLOAT);