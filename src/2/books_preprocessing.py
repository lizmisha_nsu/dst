from typing import List, Tuple, Union

import re
import pandas as pd

flatten = lambda l: [item for sublist in l for item in sublist]


def extract_float(text: str) -> Union[str, float]:
    if pd.isna(text):
        return text
    value = re.findall("\d+[\.\d+]*", text)
    return float(value[0])


def extract_list_float(text: str) -> Union[str, List[float]]:
    if pd.isna(text):
        return text
    return list(map(float, re.findall("\d+\.\d+", text)))


def extract_list_str(text: str) -> Union[str, List[str]]:
    if pd.isna(text):
        return text
    return list(set([curr_str.strip() for curr_str in re.sub('\(.*\)', ' ', text).split(',')]))


def extract_dimensions(text: str) -> pd.Series:
    if pd.isna(text):
        return pd.Series([text, text, text])

    dimensions = extract_list_float(text)
    return pd.Series([dimensions[0], dimensions[1], dimensions[2]])


def get_ids(df: pd.DataFrame, name_column: str) -> Tuple[pd.Series, List[str]]:
    names = list(set(df[name_column].values))
    return df[name_column].apply(lambda p: names.index(p)), names


def get_flatten_ids(df: pd.DataFrame, name_column: str) -> Tuple[pd.Series, List[str]]:
    names = flatten(df[name_column].values)
    names = list(set(names))
    return df[name_column].apply(lambda names_list: [names.index(name) for name in names_list]), names


def split_by(text: str, by: List[str]) -> List[str]:
    text = [text]
    for char in by:
        text = flatten(map(lambda x: x.split(char), text))
    text = list(map(lambda x: x.strip(), text))
    text = list(filter(lambda x: x != '', text))
    return text


def preprocessing_books(books: pd.DataFrame) -> Tuple[pd.DataFrame, List[str], List[str], List[str], List[str]]:
    books['price'] = books['price'].apply(extract_float)
    books['lexile'] = books['lexile'].apply(extract_float)

    books['width'], books['height'], books['depth'] = books['dimensions'].apply(extract_dimensions).T.values
    del books['dimensions']

    books['authors'] = books['authors'].apply(extract_list_str)
    books['authors_ids'], authors = get_flatten_ids(books, 'authors')
    del books['authors'], books['author']

    books['publisher_id'], publishers = get_ids(books, 'publisher')
    books['book_id'], books_titles = get_ids(books, 'title')
    del books['publisher'], books['title']

    books['subjects'] = books['subjects'].fillna('').apply(lambda x: split_by(x, by=[',', '-', '&']))
    books['subjects_ids'], subjects = get_flatten_ids(books, 'subjects')
    del books['subjects']

    return books, authors, publishers, books_titles, subjects
