#!/bin/bash

psql -h localhost -p 5432 -U postgres -W postgres -c "CREATE USER books WITH PASSWORD 'books';"
psql -h localhost -p 5432 -U postgres -W postgres -c "CREATE DATABASE books;"
psql -h localhost -p 5432 -U postgres -W postgres -c "GRANT ALL PRIVILEGES ON DATABASE books to books;"

psql -h localhost -p 5432 -U books -W books -f schema.sql
psql -h localhost -p 5432 -U books -W books -f books_data.sql

psql -h localhost -p 5432 -U books -W books -c "
    SELECT a.name, count(*)
    FROM book_to_author AS b2a
    JOIN author AS a ON a.id = b2a.author_id
    GROUP BY a.name
    ORDER BY count(*) DESC
    LIMIT 10;
"