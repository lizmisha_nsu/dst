import argparse
from typing import List

import pandas as pd
from pypika import Table, Query

from books_preprocessing import preprocessing_books


def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        '--books_path',
        type=str,
        default='../../d/books.csv'
    )
    return parser.parse_args()


def get_query(names: List[str], name: str) -> Query:
    query = Query.into(Table(name))
    for i, curr_name in enumerate(names):
        query = query.insert((i, curr_name))
    return query


def main():
    args = parse_args()

    books = pd.read_csv(args.books_path)
    books, authors, publishers, books_titles, subjects = preprocessing_books(books)

    author_query = get_query(authors, 'author')
    publisher_query = get_query(publishers, 'publisher')
    subject_query = get_query(subjects, 'subject')
    book_query = get_query(books_titles, 'book')

    book_info_query = Query.into(Table('book_info'))
    book_to_subject_q = Query.into(Table('book_to_subject')).columns(['book_id', 'subject_id'])
    book_to_author_q = Query.into(Table('book_to_author')).columns(['book_id', 'author_id'])

    for index, row in books.iterrows():
        book_info_query = book_info_query.insert((
            index,
            row['isbn10'] if not pd.isna(row['isbn10']) else 0,
            row['isbn13'] if not pd.isna(row['isbn13']) else 0,
            row['book_id'],
            row['publisher_id'],
            row['pubyear'] if not pd.isna(row['pubyear']) else 0,
            row['price'] if not pd.isna(row['price']) else 0,
            row['pages'] if not pd.isna(row['pages']) else 0,
            row['width'] if not pd.isna(row['width']) else 0,
            row['height'] if not pd.isna(row['height']) else 0,
            row['depth'] if not pd.isna(row['depth']) else 0
        ))

        for s in row['subjects_ids']:
            book_to_subject_q = book_to_subject_q.insert((row['book_id'], s))

        for a in row['authors_ids']:
            book_to_author_q = book_to_author_q.insert((row['book_id'], a))

    query = str(author_query) + ";\n\n" + str(publisher_query) + ";\n\n" + str(subject_query) + ";\n\n" \
            + str(book_query) + ";\n\n" + str(book_info_query) + ";\n\n" + str(book_to_subject_q) + ";\n\n" \
            + str(book_to_author_q) + ";"

    with open("books_data.sql", "w") as f:
        f.write(query)


if __name__ == '__main__':
    main()
