redis-cli <<EOF

SET user1 "den"
GET user1

MSET user2 "den" phone2 "+77777777777"
MGET user2 phone2

SET number 3
GET number
INCR number
GET number

MULTI
SET user3 "den"
DISCARD
MULTI
SET user3 "misha"
EXEC


HSET user4 username "den" phone "+77777777777"
HGET user4 phone
HGETALL user4


LPUSH wishlist 1 a
RPUSH wishlist 2 b
LLEN wishlist
LPOP wishlist
LLEN wishlist


SADD myset1 "den" "misha"
SMEMBERS myset1
SADD myset2 "nikita" "den" "kirill"
SMEMBERS myset2

SUNION myset1 myset2
SINTER myset1 myset2
SDIFF myset1 myset2

SUNIONSTORE uni myset1 myset2
SMEMBERS uni
SINTERSTORE inter myset1 myset2
SMEMBERS inter
SDIFFSTORE diff myset1 myset2
SMEMBERS diff

SMOVE myset1 myset2 "misha"
SMEMBERS myset1
SMEMBERS myset2

SPOP myset2 1
SMEMBERS myset2


SET user5 "kirill"
EXPIRE user5 10
EXISTS user5
EXISTS non_user
TTL user5
SETEX user5 10 "den"
TTL user5
PERSIST user5


MOVE myset2 2
SELECT 2
SMEMBERS myset2

EOF