db.createCollection("players")

db.players.insertMany([
    {
        surname: "Messi",
        name: "Lionel",
        age: 32,
        position: "FW",
        goals: 25,
        team: "Barcelona",
        capitan: true
    },{
        surname: "Rakitic",
        name: "Ivan",
        age: 32,
        position: "MF",
        goals: 12,
        team: "Barcelona",
        capitan: false
    },{
        surname: "Peque",
        name: "Gerard",
        age: 33,
        position: "DF",
        goals: 4,
        team: "Barcelona",
        capitan: false
    }
])

db.players.find()

db.players.find().skip(1).limit(1)

db.players.find({"_id" : ObjectId("5ee7be7cc2e09b9ee547444c")})