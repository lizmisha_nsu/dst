hadoop fs -mkdir /user/root/data
hadoop fs -copyFromLocal /root/dst/d/mr/tf-idf/ /user/root/data

cp -R /root/dst /home/cloudera/
chown -R cloudera:cloudera /home/cloudera/dst/

hadoop jar /usr/lib/hadoop-0.20-mapreduce/contrib/streaming/hadoop-streaming-2.6.0-mr1-cdh5.7.0.jar \
-D mapred.reduce.tasks=1 \
-input /user/root/data/tf-idf/ \
-output /user/root/data/out \
-mapper /home/cloudera/dst/src/6/mapper.py \
-reducer /home/cloudera/dst/src/6/reducer.py

hadoop fs -text /user/root/data/out/part*